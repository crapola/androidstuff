package com.example.utilisateur.myapplication;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.util.AttributeSet;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;

/*
java.lang.Object
^ android.view.View
  ^ android.view.SurfaceView
*/

class FunView extends SurfaceView implements SurfaceHolder.Callback {

    private final String tag = "FUNVIEW"; // Log tag
    private final Paint paint = new Paint(/*Paint.ANTI_ALIAS_FLAG*/);
    private SurfaceHolder holder = getHolder();
    private Bitmap buffer;
    private Canvas bufferCanvas;
    private int width,height;

    // Constructed from Java.
    public FunView(Context context) {
        super(context);
        init();
    }

    // Constructed from XML without style.
    public FunView(Context context, AttributeSet attrs) {
        super(context, attrs);
        init();
    }

    // Constructed from XML with style.
    public FunView(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();
    }

    public FunView(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();
    }

    // View
    @Override
    protected void onDraw(Canvas canvas) {
      /*  super.onDraw(canvas);/*
        paint.setColor(0xff0000ff);
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        canvas.drawRGB(100, 200, 250);
        canvas.drawCircle(20, 20, 20, paint);*/

        // canvas.drawBitmap(buffer, 0, 0, paint);
        // canvas.drawText("\uD83E\uDD14",50,50, paint);
        Log.i(tag, "onDraw");
    }

    // SurfaceHolder callbacks
    @Override
    public void surfaceCreated(SurfaceHolder surfaceHolder) {
        Log.i(tag, "surfaceCreated");

    }

    @Override
    public void surfaceChanged(SurfaceHolder surfaceHolder, int i, int i1, int i2) {
        Log.i(tag, "surfaceChanged");
        DrawBuffer();
    }

    @Override
    public void surfaceDestroyed(SurfaceHolder surfaceHolder) {
        Log.i(tag, "surfaceDestroyed");
    }

    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        int w=right-left;
        int h=bottom-top;
        Log.i(tag, "onLayout "+w+","+h+".");
        CreateBitmapBuffer(w,h);
    }

    // ---------------------------------------------------------------------------------------------

    protected void init() {
        Log.i(tag,"init");
        //CreateBitmapBuffer();
        // Holder.
        holder.addCallback(this);
        // Paint.
        paint.setTextSize(64); // Size in pixels.

    }

    void CreateBitmapBuffer(int width,int height)
    {
        // Create bitmap.
        Bitmap.Config config = Bitmap.Config.ARGB_8888;
        buffer = Bitmap.createBitmap(width,height, config);
        bufferCanvas = new Canvas(buffer);
        // Blueish background.
        bufferCanvas.drawRGB(220,230,240);

    }

    void DrawStuff(int x, int y) {
        bufferCanvas.drawCircle(x, y, 20, paint);
        bufferCanvas.drawText("\uD83E\uDD14Yo", x, y, paint);
        DrawBuffer();
    }

    void DrawBuffer()
    {
        Canvas c = holder.lockCanvas();
        c.drawBitmap(buffer, 0, 0, paint);
        holder.unlockCanvasAndPost(c);
    }

}

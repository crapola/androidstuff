package com.example.utilisateur.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
//import android.os.Handler;

public class MainActivity extends AppCompatActivity {

    public static final String EXTRA_MESSAGE = "succ";

    /*
    Called when app start.
    saveInstanceState is null on first launch.
    */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
    }

    @Override
    protected void onResume() {
        super.onResume();
        Log.i("Info","Resumed");
    }

    public void leButton(View v) {
        /*
        R.java is the dynamically generated class, created during build process to dynamically
        identify all assets (from strings to android widgets to layouts), for usage in java classes
        in Android app.
        */
        final TextView t = findViewById(R.id.textView);
        t.setText("Wow!");
        final ImageView d = findViewById(R.id.droid);

        Animation a = AnimationUtils.loadAnimation(this, R.anim.animz);
        d.startAnimation(a);

/*
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                t.setText("Hello!");
            }
        }, 2000);*/
    }

    public void sendMessage(View butt) {
        // Tutorial stuff
        Intent intent = new Intent(this, DisplayMessageActivity.class);
        EditText editText = (EditText) findViewById(R.id.editText);
        String message = editText.getText().toString();
        intent.putExtra(EXTRA_MESSAGE, message);
        startActivity(intent);
    }
}

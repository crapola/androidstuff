package com.example.utilisateur.myapplication;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MotionEvent;
import android.widget.TextView;

public class DisplayMessageActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_display_message);

        // Get the Intent that started this activity and extract the string
        Intent intent = getIntent();
        String message = intent.getStringExtra(MainActivity.EXTRA_MESSAGE);

        // Capture the layout's TextView and set the string as its text
        TextView textView = findViewById(R.id.textView2);
        textView.setText("Hello, "+message+", draw in the box.");
    }

    @Override
    public boolean onTouchEvent(MotionEvent event)
    {
        int x = (int)event.getRawX();
        int y = (int)event.getRawY();
        switch (event.getAction()) {
            case MotionEvent.ACTION_MOVE:
                Log.i("222","X="+x+" Y="+y);
                TextView textView = findViewById(R.id.textView2);
                textView.setText("X="+x+" Y="+y);
                FunView fun=findViewById(R.id.surfaceView);
                int[] loc=new int[2];
                fun.getLocationOnScreen(loc);
                fun.DrawStuff(x-loc[0],y-loc[1]);
         }
        return false;
    }
}

@echo off

if [%1]==[] (
echo You must supply a source folder.
pause
exit
)

if not exist %1 (
echo Invalid path: %1.
pause
exit
)

set source=%1

rem Stage location
set stage="D:\Users\Utilisateur\GitStage\AndroidStuff"

for /d %%x in (%source%) do set name=%%~nx

echo Project name : %name%
echo Source : %source%
echo Stage: %stage%

set dest="%stage:"=%\%name:"=%"
echo Destination: %dest%

rem Use /NOCOPY for tests
robocopy %source% %dest% /S /XO /V /XD .gradle build bin gen out /XF *.apk *.ap_ *.class *.dex *.log *.iml .idea/workspace.xml .idea/tasks.xml .idea/gradle.xml .idea/assetWizardSettings.xml .idea/dictionaries .idea/libraries .idea/cacheslocal.properties /PURGE
pause